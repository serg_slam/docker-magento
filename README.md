# docker-magento

## Getting started

```
git clone this repo
cd repo folder
cd nginx/ssl
```

Сгенерируйте самоподписный сертификат и пропишите пути в vhost.conf

## Сборка и запуск контейнера

```
cd repo folder
docker-compose build && docker-compose up -d
```

## Установка зависимостей для web-приложений.

Если для работы web-приложений необходимо установить зависимости, например через менеджер пакетов Composer или NPM, то сейчас самое время сделать это.

В контейнере php-7.4 уже установлен и Composer и NPM.

Войдите в контейнер php-7.4:

docker exec -it php-7.4 bash

Перейдите в рабочий каталог необходимого web-проекта и выполните требуемые действия.

Например, установите зависимости через Composer при помощи команды:
composer install
